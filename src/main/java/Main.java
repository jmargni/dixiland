import com.rumbo.dixiland.algorithms.DijkstraSP;
import com.rumbo.dixiland.algorithms.DirectedDFS;
import com.rumbo.dixiland.graph.Itinerary;
import com.rumbo.dixiland.graph.SymbolEdgeWeightedDigraph;

import java.util.ArrayList;


public class Main
{
  public static final String PATH_SEPARATOR = "-";

  public static void main(String[] args)
  {
    SymbolEdgeWeightedDigraph SG = new SymbolEdgeWeightedDigraph("/dixiland_data.txt", " ");

    System.out.println("Scenari Dixiland\n\n");

    output_scenario_1(SG);
    output_scenario_2(SG);
    output_scenario_3(SG);
    output_scenario_4(SG);
    output_scenario_5(SG);
    output_scenario_6(SG);
    output_scenario_7(SG);
    output_scenario_8(SG);
    output_scenario_9(SG);
    output_scenario_10(SG);
  }

  private static void output_scenario_1(SymbolEdgeWeightedDigraph SG)
  {
    System.out.println("Scenario 1: Tempo di percorrenza dell'itinerario M-N-O");

    String stringPath = "M-N-O";
    Itinerary itinerary = new Itinerary(SG, stringPath, PATH_SEPARATOR);

    System.out.println("Tempo = " + itinerary.getPathWeight());
  }

  private static void output_scenario_2(SymbolEdgeWeightedDigraph SG)
  {
    System.out.println("\nScenario 2: Tempo di percorrenza dell'itinerario M-P");

    String stringPath = "M-P";
    Itinerary itinerary = new Itinerary(SG, stringPath, PATH_SEPARATOR);

    System.out.println("Tempo = " + itinerary.getPathWeight());
  }

  private static void output_scenario_3(SymbolEdgeWeightedDigraph SG)
  {
    System.out.println("\nScenario 3: Tempo di percorrenza dell'itinerario M-P-O");

    String stringPath = "M-P-O";
    Itinerary itinerary = new Itinerary(SG, stringPath, PATH_SEPARATOR);

    System.out.println("Tempo = " + itinerary.getPathWeight());
  }

  private static void output_scenario_4(SymbolEdgeWeightedDigraph SG)
  {
    System.out.println("\nScenario 4: Tempo di percorrenza dell'itinerario M-Q-N-O-P");

    String stringPath = "M-Q-N-O-P";
    Itinerary itinerary = new Itinerary(SG, stringPath, PATH_SEPARATOR);

    System.out.println("Tempo = " + itinerary.getPathWeight());
  }

  private static void output_scenario_5(SymbolEdgeWeightedDigraph SG)
  {
    System.out.println("\nScenario 5: Tempo di percorrenza dell'itinerario M-Q-P");

    String stringPath = "M-Q-P";
    Itinerary itinerary = new Itinerary(SG, stringPath, PATH_SEPARATOR);
    String weight = (String) ((itinerary.getPathWeight() != null) ? itinerary.getPathWeight() : "Nessun percorso trovato");
    System.out.println("Tempo = " + weight);
  }

  private static void output_scenario_6(SymbolEdgeWeightedDigraph SG)
  {
    int start = SG.index("O");
    int end = start;
    int stops = 2;
    DirectedDFS dfs = new DirectedDFS(SG.G(), start);

    System.out.println("\nScenario 6: Numero di itinerari che partono da O e arrivano a O con massimo 2 scali");

    ArrayList<Itinerary> paths = dfs.getPathsWhithAtLeastNStops(end, stops);

    System.out.println("Numero di itinerari: " + paths.size());

    for (Itinerary i : paths)
    {
      System.out.println(i.getPathAsStringLinkedList(SG));
    }
  }

  private static void output_scenario_7(SymbolEdgeWeightedDigraph SG)
  {
    int start = SG.index("M");
    int end = SG.index("O");
    int stops = 3;
    DirectedDFS dfs = new DirectedDFS(SG.G(), start);

    System.out.println("\nScenario 7: Numero di percorsi che partono da M e arrivano a O con essatamente 3 scali");

    ArrayList<Itinerary> paths = dfs.getPathsWhithExactNStops(end, stops);

    System.out.println("Numero di percorsi: " + paths.size());

    for (Itinerary i : paths)
    {
      System.out.println(i.getPathAsStringLinkedList(SG));
    }
  }

  private static void output_scenario_8(SymbolEdgeWeightedDigraph SG)
  {
    System.out.println("\nScenario 8: Tempo di percorrenza piu veloce dell'itinerario da M a O");

    int start = SG.index("M");
    int end = SG.index("O");

    DijkstraSP dijkstraSP = new DijkstraSP(SG.G(), start);

    System.out.println("Tempo: " + dijkstraSP.distTo(end));
  }

  private static void output_scenario_9(SymbolEdgeWeightedDigraph SG)
  {
    System.out.println("\nScenario 9: Tempo di percorrenza piu veloce dell'itinerario da M a N");

    int start = SG.index("M");
    int end = SG.index("N");

    DijkstraSP dijkstraSP = new DijkstraSP(SG.G(), start);

    System.out.println("Tempo: " + dijkstraSP.distTo(end));

  }

  private static void output_scenario_10(SymbolEdgeWeightedDigraph SG)
  {
    System.out.println("\nScenario 10: numero di percorsi differenti da O a O con una distanza inferiore a 30");

    int start = SG.index("O");
    int end = SG.index("O");
    DirectedDFS dfs = new DirectedDFS(SG.G(), start);

    ArrayList<Itinerary> paths = dfs.getPathsWhithWeightLessThan(30, end);

    System.out.println("Numero di percorsi: " + paths.size());

    for (Itinerary i : paths)
    {
      System.out.println(i.getPathWeight() + " \t" + i.getPathAsStringLinkedList(SG));
    }
  }
}
