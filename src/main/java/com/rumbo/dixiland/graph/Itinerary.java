package com.rumbo.dixiland.graph;

import java.util.LinkedList;

/**
 * Created by jmargni on 16/06/14.
 */

public class Itinerary implements Comparable<Itinerary>
{
  private EdgeWeightedDigraph G;
  private LinkedList<Integer> path;

  public Itinerary(EdgeWeightedDigraph G, LinkedList<Integer> path)
  {
    init(G, path);
  }

  public Itinerary(SymbolEdgeWeightedDigraph SG, String stringPath, String separator)
  {
    LinkedList<Integer> linkedPath = new LinkedList<Integer>();

    String[] v = stringPath.split(separator);
    for (int i = 0; i < v.length; i++)
    {
      linkedPath.add(SG.index(v[i]));
    }

    init(SG.G(), linkedPath);
  }

  private void init(EdgeWeightedDigraph G, LinkedList<Integer> linkedPath)
  {
    this.G = G;
    this.path = (linkedPath != null) ? new LinkedList<>(linkedPath) : new LinkedList<Integer>();
  }

  public LinkedList<Integer> getPathAsIntegerLinkedList()
  {
    return this.path;
  }

  public LinkedList<String> getPathAsStringLinkedList(SymbolEdgeWeightedDigraph SG)
  {
    LinkedList<String> stringPath = new LinkedList<>();
    for (int i : getPathAsIntegerLinkedList())
    {
      stringPath.add(SG.name(i));
    }
    return stringPath;
  }

  public void addNode(int v)
  {
    this.path.add(v);
  }

  public Double getPathWeight()
  {
    Double totalWeight = 0D;

    LinkedList<Integer> clonedPath = (LinkedList<Integer>) this.path.clone();

    int v = clonedPath.pop();
    for (Integer w : clonedPath)
    {
      Double weight = hasEdge(v, w);
      if (weight == null)
      {
        return null;
      }
      totalWeight += weight;
      v = w;
    }
    return totalWeight;
  }

  private Double hasEdge(int v, int w)
  {
    for (DirectedEdge edge : G.adj(v))
    {
      if (edge.to() == w)
      {
        return edge.weight();
      }
    }
    return null;
  }

  @Override
  public int compareTo(Itinerary that)
  {
    final int EQUAL = 0;

    if (this == that) return EQUAL;

    return this.getPathWeight().compareTo(that.getPathWeight());
  }
}





