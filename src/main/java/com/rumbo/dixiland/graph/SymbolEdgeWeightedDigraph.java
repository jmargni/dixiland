package com.rumbo.dixiland.graph;


import com.rumbo.dixiland.io.In;

import java.util.HashMap;

public class SymbolEdgeWeightedDigraph
{
  private HashMap<String, Integer> st; // String -> index
  private String[] keys; // index -> String
  private EdgeWeightedDigraph G; // the graph

  public SymbolEdgeWeightedDigraph(String stream, String sp)
  {
    st = new HashMap<String, Integer>();

    // Builds the index
    In in = new In(stream); // First pass
    while (in.hasNextLine()) // builds the index
    {
      String[] a = in.readLine().split(sp); // by reading strings
      for (int i = 0; i < a.length -1; i++) // to associate each
        if (st.get(a[i]) == null) // distinct string
          st.put(a[i], st.size()); // with an index.
    }

    // Builds inverted index
    keys = new String[st.size()]; // Inverted index
    for (String name : st.keySet()) // to get string keys
      keys[st.get(name)] = name; // is an array.

    // Builds the graph
    G = new EdgeWeightedDigraph(st.size());
    in = new In(stream); // Second pass
    while (in.hasNextLine()) // builds the graph
    {
      String[] a = in.readLine().split(sp); // by connecting the

      int v = st.get(a[0]); // first vertex
      int w = st.get(a[1]); // second vertex
      double weight = Double.valueOf(a[2]); // weight
      G.addEdge(new DirectedEdge(v, w, weight));
    }
  }

  public boolean contains(String s)
  {
    return !(st.get(s) == null);
  }

  public int index(String s)
  {
    return st.get(s);
  }

  public String name(int v)
  {
    return keys[v];
  }

  public EdgeWeightedDigraph G()
  {
    return G;
  }
}