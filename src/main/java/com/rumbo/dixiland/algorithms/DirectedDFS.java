package com.rumbo.dixiland.algorithms;

import com.rumbo.dixiland.graph.DirectedEdge;
import com.rumbo.dixiland.graph.EdgeWeightedDigraph;
import com.rumbo.dixiland.graph.Itinerary;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by jmargni on 6/17/14.
 */

public class DirectedDFS
{
  private final EdgeWeightedDigraph G;
  private LinkedList<Integer> path;
  private ArrayList<Itinerary> paths;
  int s;

  public DirectedDFS(EdgeWeightedDigraph G, int s)
  {
    this.G = G;
    this.s = s;
  }

  public ArrayList<Itinerary> getAllDirectPathsTo(int v)
  {
    exploreInit();

    for (DirectedEdge edge : G.adj(s)) {
      exploreDirectPaths(edge, v);
    }
    return paths;
  }

  public ArrayList<Itinerary> getPathsWhithAtLeastNStops(int v, int stops)
  {
    exploreInit();

    for (DirectedEdge edge : G.adj(s)) {
      exploreAtLeastNStops(edge, v, stops);
    }
    return paths;
  }

  public ArrayList<Itinerary> getPathsWhithExactNStops(int v, int stops)
  {
    exploreInit();

    for (DirectedEdge edge : G.adj(s)) {
      exploreExactNStops(edge, v, stops);
    }
    return paths;
  }

  public ArrayList<Itinerary> getPathsWhithWeightLessThan(int weight, int v)
  {
    exploreInit();

    for (DirectedEdge edge : G.adj(s)) {
      exploreByLessWeight(edge, v, weight);
    }
    return paths;
  }

  private void exploreInit()
  {
    paths = new ArrayList<>();
    path = new LinkedList<>();
    path.add(s);
  }

  private void exploreDirectPaths(DirectedEdge e, int v)
  {
    // Check if destination node is reached, if yes, save the path
    if (e.to() == v) {
      LinkedList<Integer> foundPath = (LinkedList<Integer>) path.clone();
      foundPath.add(e.to());
      paths.add(new Itinerary(G, foundPath));
    }
    // Avoid loops passing over nodes present in path
    if (path.contains(e.to())) {
      return;
    }
    // Add node to path and go avanti exploring :-)
    if (e.to() == v) return;
    path.add(e.to());
    for (DirectedEdge edge : G.adj(e.to())) {
      exploreDirectPaths(edge, v);
    }
    // Backtrack explored path
    path.removeLast();
  }

  private void exploreAtLeastNStops(DirectedEdge e, int v, int stops)
  {
    // Check if destination node is reached, if yes, save the path
    if (e.to() == v && path.size() <= stops + 1) {
      LinkedList<Integer> foundPath = (LinkedList<Integer>) path.clone();
      foundPath.add(e.to());
      paths.add(new Itinerary(G, foundPath));
    }
    // Add node to path and go avanti exploring :-)
    if (path.size() > stops + 1) return;
    path.add(e.to());
    for (DirectedEdge edge : G.adj(e.to())) {
      exploreAtLeastNStops(edge, v, stops);
    }
    // Backtrack explored path
    path.removeLast();
  }

  private void exploreExactNStops(DirectedEdge e, int v, int stops)
  {
    // Check if destination node is reached, if yes, save the path
    if (e.to() == v && path.size() == stops + 1) {
      LinkedList<Integer> foundPath = (LinkedList<Integer>) path.clone();
      foundPath.add(e.to());
      paths.add(new Itinerary(G, foundPath));
    }
    // Add node to path and go avanti exploring :-)
    if (path.size() > stops + 1) return;
    path.add(e.to());
    for (DirectedEdge edge : G.adj(e.to())) {
      exploreExactNStops(edge, v, stops);
    }
    // Backtrack explored path
    path.removeLast();
  }

  private void exploreByLessWeight(DirectedEdge e, int v, int weight)
  {
    // Check if destination node is reached, if yes, save the path
    Itinerary itinerary = new Itinerary(G, path);
    itinerary.addNode(e.to());
    if (e.to() == v && itinerary.getPathWeight() <  weight) {
      LinkedList<Integer> foundPath = (LinkedList<Integer>) path.clone();
      foundPath.add(e.to());
      paths.add(new Itinerary(G, foundPath));
      //return;
    }
    // Add node to path and go avanti exploring :-)
    if (itinerary.getPathWeight() > weight) return;
    path.add(e.to());
    for (DirectedEdge edge : G.adj(e.to())) {
      exploreByLessWeight(edge, v, weight);
    }
    // Backtrack explored path
    path.removeLast();
  }
}
