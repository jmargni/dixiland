package com.rumbo.dixiland;

import com.rumbo.dixiland.algorithms.DijkstraSP;
import com.rumbo.dixiland.algorithms.DirectedDFS;
import com.rumbo.dixiland.graph.Itinerary;
import com.rumbo.dixiland.graph.SymbolEdgeWeightedDigraph;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;


public class DixilandTest
{

  private static final String SEPARATOR = "-";
  private SymbolEdgeWeightedDigraph SG;

  @Before
  public void setUp() throws Exception
  {
    SG = new SymbolEdgeWeightedDigraph("/dixiland_data.txt", " ");

  }

  // Scenario 1
  @Test
  public void given_Itinerary_MNO__travel_Time_Is_9() throws Exception
  {
    String stringPath = "M-N-O";
    Itinerary itinerary = new Itinerary(SG, stringPath, SEPARATOR);

    double expectedValue = 9;

    assertThat(expectedValue, equalTo(itinerary.getPathWeight()));
  }

  // Scenario 2
  @Test
  public void given_Itinerary_MP__travel_Time_Is_5() throws Exception
  {
    String stringPath = "M-P";
    Itinerary itinerary = new Itinerary(SG, stringPath, SEPARATOR);

    double expectedValue = 5;

    assertThat(expectedValue, equalTo(itinerary.getPathWeight()));
  }

  // Scenario 3
  @Test
  public void given_Itinerary_MPO__travel_Time_Is_13() throws Exception
  {
    String stringPath = "M-P-O";
    Itinerary itinerary = new Itinerary(SG, stringPath, SEPARATOR);

    double expectedValue = 13;

    assertThat(expectedValue, equalTo(itinerary.getPathWeight()));
  }

  // Scenario 4
  @Test
  public void given_Itinerary_MQNOP__travel_Time_Is_22() throws Exception
  {
    String stringPath = "M-Q-N-O-P";
    Itinerary itinerary = new Itinerary(SG, stringPath, SEPARATOR);

    double expectedValue = 22;

    assertThat(expectedValue, equalTo(itinerary.getPathWeight()));
  }

  // Scenario 5
  @Test
  public void given_Itinerary_MQP__travel_Time_Is_null() throws Exception
  {
    String stringPath = "M-Q-P";
    Itinerary itinerary = new Itinerary(SG, stringPath, SEPARATOR);

    Double expectedValue = null;

    assertThat(expectedValue, equalTo(itinerary.getPathWeight()));
  }

  // Scenario 6
  @Test
  public void given_StartAirport_O_And_EndAirport_O__there_Are_2_Paths_With_At_Least_2_Stops() throws Exception
  {
    int start = SG.index("O");
    int stops = 2;

    DirectedDFS dfs = new DirectedDFS(SG.G(), start);

    int expectedValue = 2;

    ArrayList<Itinerary> paths = dfs.getPathsWhithAtLeastNStops(start, stops);

    assertThat(expectedValue, equalTo(paths.size()));
  }

  // Scenatio 7
  @Test
  public void given_StartAirport_M_And_EndAirport_O__there_Are_3_Paths_With_Exact_3_stops()
  {
    int start = SG.index("M");
    int end = SG.index("O");

    DirectedDFS dfs = new DirectedDFS(SG.G(), start);

    int expectedValue = 3;

    ArrayList<Itinerary> paths = dfs.getPathsWhithExactNStops(end, 3);

    assertThat(expectedValue, equalTo(paths.size()));
  }

  // Scenario 8
  @Test
  public void given_StartAirport_M_And_EndAirport_O__shortest_Path_Weight_Is_9() throws Exception
  {
    int start = SG.index("M");
    int end = SG.index("O");

    DijkstraSP dijkstraSP = new DijkstraSP(SG.G(), start);
    Double expectedValue = 9D;

    assertThat(expectedValue, equalTo(dijkstraSP.distTo(end)));
  }

  // Scenario 9
  @Test
  public void given_StartAirport_M_And_EndAirport_N__shortest_Path_Weight_Is_5() throws Exception
  {
    int start = SG.index("M");
    int end = SG.index("N");

    DijkstraSP dijkstraSP = new DijkstraSP(SG.G(), start);
    Double expectedValue = 5D;

    assertThat(expectedValue, equalTo(dijkstraSP.distTo(end)));
  }

  // Scenario 10
  @Test
  public void given_StartAirport_O_And_EndAirport_O__there_are_7_Paths_With_Weight_Less_Than_30() throws Exception
  {
    int start = SG.index("O");
    int end = SG.index("O");

    DirectedDFS dfs = new DirectedDFS(SG.G(), start);

    int expectedValue = 7;

    ArrayList<Itinerary> paths = dfs.getPathsWhithWeightLessThan(30, end);

    assertThat(expectedValue, equalTo(paths.size()));
  }
}
