package com.rumbo.dixiland.graph;

import com.rumbo.dixiland.io.In;
import com.rumbo.dixiland.structure.Bag;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class EdgeWeightedDigraphTest
{
  @Test
  public void givenInputFile_DigraphIsCreated() throws Exception
  {
    String fileName = "/tinyEWD.txt";

    int expectedEdgeNumber = 15;

    EdgeWeightedDigraph graph = new EdgeWeightedDigraph(new In(fileName));

    assertThat(((Bag)graph.edges()).size(), is(expectedEdgeNumber));
  }
}