package com.rumbo.dixiland.graph;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class DirectedEdgeTest {

    @Test
    public void directedEdgeCreationTest() throws Exception {
        DirectedEdge directedEdge = new DirectedEdge(1,2,3D);

        assertThat(directedEdge.from(), equalTo(1));
        assertThat(directedEdge.to(), equalTo(2));
        assertThat(directedEdge.weight(), equalTo(3D));
    }

    @Test
    public void directedEdgeToStringTest() throws Exception {
        DirectedEdge directedEdge = new DirectedEdge(1,2,3D);

        assertThat(directedEdge.toString(), equalTo("1->2 3.00"));
    }
}